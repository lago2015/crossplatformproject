﻿using UnityEngine;
using System.Collections;

public class obstacleMovement : MonoBehaviour {

    public Vector3 speed;
    public float lifeSpan = 10;
    // Use this for initialization
    void Start()
    {
        //random = Random.insideUnitSphere * 15;
        //transform.position = random;
        //transform.position = random(Random.insideUnitSphere * 15);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Destroy(gameObject, lifeSpan);
        moveSpeed(speed);
    }
    public Vector3 moveSpeed(Vector3 movement)
    {
        Vector3 speed = GameObject.FindGameObjectWithTag("Player").GetComponent<playerMechanics>().movement;
        this.transform.position += movement * Time.deltaTime;
        
        return movement;
    }
}
