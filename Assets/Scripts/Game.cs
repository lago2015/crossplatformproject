﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{

    ////GUI Variables
    //public GUIText GUITimer;
    public float timer = 60;
    public float elapsedTime = 0;
    public float tunnelSpawnRate = 5;
    public double distance = 0;

    //pillar time
    public float pillTime = 0;
    public float pillRate=5;
    bool changeRot=false;
    public GameObject pillar;
    public Transform pillarSpawn;

    //object spawning
    public GameObject border1;
    public GameObject border2;
    bool tunSwitch = false;
    public Transform BorderSpawnPoint;
    bool isSpawning = false;
    public float minTime=1;
    public float maxTime = 3;
    public Transform[] obstacleSpawn;
    public GameObject[] obstacles;  // Array of enemy prefabs.

    //pick up variables
    public float pickUpTimer=0;
    public float pickUpRate=20;
    public GameObject[] pickups;    //array for pickups
    public Transform pickUpSpawn;

    // Use this for initialization
    void Start()
    {
       //StartCoroutine(spawn());
    }
    void FixedUpdate()
    {
        float speed = GameObject.FindGameObjectWithTag("Player").GetComponent<playerMechanics>().movement.z;

        //timers
        elapsedTime += Time.deltaTime;
        timer -= Time.deltaTime;
        pillTime += Time.deltaTime;
        pickUpTimer += Time.deltaTime;
        distance += speed * 10 * Time.deltaTime;
        int pickUpIndex = Random.Range(0, pickups.Length);

        //pillar spawn rate
        if(pillTime>=pillRate)
        {
            if(changeRot==true)
            {
                Instantiate(pillar, pillarSpawn.position, Quaternion.identity);
                changeRot = false;
            }
            else if (changeRot==false)
            {
                Instantiate(pillar, pillarSpawn.position, Quaternion.Euler(0, 0, 90));
                changeRot = true;
            }
            pillTime = 0;
        }
        //pick up spawn rate
        if(pickUpTimer>=pickUpRate)
        {
            Instantiate(pickups[pickUpIndex], pickUpSpawn.position, Quaternion.identity);
            pickUpTimer = 0;
        }
        //tunnel spawn rate
        if(elapsedTime>tunnelSpawnRate)
        {
            if(tunSwitch==false)
            {
                Instantiate(border1, BorderSpawnPoint.position, BorderSpawnPoint.rotation);
                tunSwitch = true;
            }
            else if(tunSwitch==true)
            {
                Instantiate(border2, BorderSpawnPoint.position, BorderSpawnPoint.rotation);
                tunSwitch = false;
            }
            elapsedTime = 0;
            
        }
        
        //obstacle spawn rate
        if (!isSpawning)
        {
            isSpawning = true; 
            int enemyIndex = Random.Range(0, obstacles.Length);
            StartCoroutine(spawn(enemyIndex, Random.Range(minTime, maxTime)));
        }
    }
    IEnumerator spawn(int index, float seconds)
    {
        int spawnIndex = Random.Range(0, obstacleSpawn.Length);
        yield return new WaitForSeconds(Random.Range(minTime, maxTime));
        Instantiate(obstacles[index], obstacleSpawn[spawnIndex].position, Quaternion.identity);
        isSpawning = false;
    }
    void OnGUI()
    {
        GUI.contentColor = Color.green;
        GUI.Box(new Rect(Screen.width /2 -100, 5, 200, 30), "Timer: " + System.Math.Round(timer));
        if (timer <= 0)
        {
            GUI.Box(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 100, 100, 50), "Times Up!");
            Time.timeScale = 0;
        }
        float speed = GameObject.FindGameObjectWithTag("Player").GetComponent<playerMechanics>().movement.z;
        GUI.Box(new Rect(0,0,200,30),"Speed: "+speed);
        GUI.Box(new Rect(Screen.width - 200, 0, 200, 30),"D: "+ System.Math.Round(distance) +" m");
    }

}
