﻿using UnityEngine;
using System.Collections;

public class playerMechanics : MonoBehaviour {

    int count = 0;
    //bool status of pickups
    bool motorBoost;
    bool motorDecrease;
    bool confuse;
    bool speedBoost;
    float speedTime=0;
    float speedUp=5;
    public float speed;
    public int speedLimit = 15;
    public int rotateSpeed = 100;
    public Vector3 movement;
    public float buffTimer=0;
    public float buffDuration=5;
    public float debuffTimer = 0;
    public float debuffDuration=5;
	// Use this for initialization
	void Start () 
    {
        motorBoost = false;
        motorDecrease = false;
        confuse = false;
        speedBoost = false;

        if (!GetComponent<NetworkView>().isMine)
        {
            Component.Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        //confuse function
        if (confuse == true)
        {
            if (debuffTimer <= debuffDuration)
            {
                debuffTimer += Time.deltaTime;
                if (Input.GetKey("a"))
                    transform.RotateAround(Vector3.zero, Vector3.forward, rotateSpeed * Time.deltaTime);
                else if (Input.GetKey("d"))
                    transform.RotateAround(Vector3.zero, -Vector3.forward, rotateSpeed * Time.deltaTime);
            }
            else if (debuffTimer >= debuffDuration)
            {
                debuffTimer = 0;
                confuse = false;
            }
        }
        else if(confuse==false)
        {
            if (Input.GetKey("d"))
                transform.RotateAround(Vector3.zero, Vector3.forward, rotateSpeed * Time.deltaTime);
            else if (Input.GetKey("a"))
                transform.RotateAround(Vector3.zero, -Vector3.forward, rotateSpeed * Time.deltaTime);
        }
	}
    void FixedUpdate()
    {
        //transform.Rotate(180 * Time.deltaTime, 0, 0);
        this.transform.position += movement * Time.deltaTime;

        if (movement.z <= speedLimit)
        {
            speedTime += Time.deltaTime;
        }
        if (movement.z >= speedLimit)
            movement.z = speedLimit;
        if(speedTime>=speedUp)
        {
            movement.z++;
            speedTime = 0;
        }
        Debug.Log("Rotation: " + rotateSpeed);
        if (buffTimer >= 10)
            buffTimer = 0;

        //motorDecrease function
        if (motorDecrease == true)
        {
            if (debuffTimer <= debuffDuration)
            {
                debuffTimer += Time.deltaTime;
                rotateSpeed = 50;
            }
            if (debuffTimer >= debuffDuration)
            {
                debuffTimer = 0;
                rotateSpeed = 100;
                motorDecrease = false;
            }
        }

        //motorIncrease function
        if (motorBoost == true)
        {
            
            if (buffTimer <= buffDuration)
            {
                buffTimer += Time.deltaTime;
                rotateSpeed = 150;
            }
            else if (buffTimer >= buffDuration)
            {
                buffTimer = 0;
                rotateSpeed = 100;
                motorBoost = false;
            }
        }

        
        //speedBoost function
        if(speedBoost==true)
        {
             Vector3 moveSpeed = GameObject.FindGameObjectWithTag("Obstacle").GetComponent<obstacleMovement>().moveSpeed(new Vector3());
             if (buffTimer <= buffDuration)
            {
                buffTimer += Time.deltaTime;
                movement.z = 15;
                gameObject.GetComponent<Collider>().isTrigger = false;
            }
             else if (buffTimer >= buffDuration)
            {
                gameObject.GetComponent<Collider>().isTrigger = true;
                movement.z = 1;
                buffTimer = 0;
                speedBoost = false;
                moveSpeed = new Vector3(0, 0, -40);
            }
        }
    }
    //trigger detection of obstacle and pick ups
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Obstacle")
        {
            count += 1;
            Debug.Log("Count: " + count);
            movement.z -= 1;
            speedLimit-=1;
        }
        if (col.gameObject.tag == "motorBoost")
        {
            motorBoost = true;
        }
        if(col.gameObject.tag=="motorDecrease")
        {
            motorDecrease = true;
        }
        if (col.gameObject.tag == "confuse")
            confuse = true;
        if (col.gameObject.tag == "speedBoost")
            speedBoost = true;
    }
    void OnGUI()
    {
        GUI.contentColor = Color.green;
        GUI.Box(new Rect(Screen.width - 200, 100, 200, 30), "Motor Boost: " + motorBoost);
        GUI.Box(new Rect(Screen.width - 200, 150, 200, 30), "Motor Decrease: " + motorDecrease);
        GUI.Box(new Rect(Screen.width - 200, 200, 200, 30), "Confusion: " + confuse);
        GUI.Box(new Rect(Screen.width - 200, 250, 200, 30), "Speed Boost/Invinsability: " + speedBoost);
        if (motorBoost == true)
        {
            GUI.contentColor = Color.red;
            GUI.Box(new Rect(Screen.width - 200, 100, 200, 30), "Motor Boost: " + motorBoost);
        }
        if (motorDecrease == true)
        {
            GUI.contentColor = Color.red;
            GUI.Box(new Rect(Screen.width - 200, 150, 200, 30), "Motor Decrease: " + motorDecrease);
        }
        if (confuse == true)
        {
            GUI.contentColor = Color.red;
            GUI.Box(new Rect(Screen.width - 200, 200, 200, 30), "Confusion: " + confuse);
        }
        if (speedBoost == true)
        {
            GUI.contentColor = Color.red;
            GUI.Box(new Rect(Screen.width - 200, 250, 200, 30), "Speed Boost/Invinsability: " + speedBoost);
        }

    }
}
