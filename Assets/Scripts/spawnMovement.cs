﻿using UnityEngine;
using System.Collections;

public class spawnMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
        if (!GetComponent<NetworkView>().isMine)
        {
            Component.Destroy(this);
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, 100 * Time.deltaTime);
	}
}
