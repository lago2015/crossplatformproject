﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
    public GameObject canvas;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void resume()
    {
        Time.timeScale = 1;
        canvas.SetActive(false);
    }
    public void ChangeToScene(int sceneToChange)
    {
        Application.LoadLevel(sceneToChange);
    }
    public void QuitGame()
    {
        Camera.main.enabled = false;
        Application.Quit();
    }
}
