﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour
{
    string registeredGameName = "VGP142_NetworkExample_Ali";
    private float requestLength = 3;
    HostData[] hosts = null;


    // Use this for initialization
    void Start()
    {
        Application.runInBackground = true;
        Time.timeScale = 0;
    }


    void OnGUI()
    {

        if (Network.isServer)
        {
            return;
        }

        if (Network.isClient)
        {
            if (GUI.Button(new Rect(25, 25, 200, 30), "Spawn Player"))
            {
                SpawnPlayer();
                Time.timeScale = 1;
            }
            return;
        }
        if (GUI.Button(new Rect(25, 25, 200, 30), "Start Server"))
        {
            StartServer();
            SpawnPlayer();
            Time.timeScale = 1;
        }

        if (GUI.Button(new Rect(25, 65, 200, 30), "Get Server List"))
        {
            StartCoroutine("GetServerList");
            
        }


        if (hosts != null && hosts.Length > 0)
        {
            if (GUI.Button(new Rect(25, 105, 200, 30), hosts[0].gameName))
            {
                print(Network.Connect(hosts[0]).ToString());
                SpawnPlayer();
                //Time.timeScale = 1;
            }
        }


    }

    public IEnumerator GetServerList()
    {
        Time.timeScale = 1;
        MasterServer.RequestHostList(registeredGameName);
        float timeStarted = Time.time;
        float timeEnd = Time.time + requestLength;

        while (Time.time < timeEnd)
        {
            hosts = MasterServer.PollHostList();
            yield return new WaitForEndOfFrame();
        }

    }

    private void StartServer()
    {
        Network.InitializeServer(16, 25002, false);
        MasterServer.RegisterHost(registeredGameName, "PitFalling Game!", "VGP142");
    }

    void OnServerInitialized()
    {
        Debug.Log("Server Initialized");
    }

    void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        if (msEvent == MasterServerEvent.RegistrationSucceeded)
        {
            print("Registration Successful");
        }
        else
        {
            print(msEvent.ToString());
        }
    }

    void OnConnectedToServer()
    {
        Time.timeScale = 1;
        print("OnConnectedToServer");
    }
    void OnDisconnectedFromServer(NetworkDisconnection dc)
    {
        print("OnDisconnectedFromServer--" + dc.ToString());
    }

    void FailedToConnect(NetworkConnectionError error)
    {
        print("FailedToConnect--" + error.ToString());
    }

    void FailedToConnectToMasterServer(NetworkConnectionError error)
    {
        print("FailedToConnectToMasterServer--" + error.ToString());
    }

    void OnNetworkInstantiate(NetworkMessageInfo info)
    {

        print("OnNetworkInstantiate--" + info.ToString());
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        //remove rpcs and destroy object
    }

    void OnApplicationQuit()
    {
        if (Network.isServer)
        {
            Network.Disconnect();
            MasterServer.UnregisterHost();
        }

    }

    private void SpawnPlayer()
    {
       Network.Instantiate(Resources.Load("Player"), new Vector3(0, -9, -83), Quaternion.identity, 0);
       
    }
}
